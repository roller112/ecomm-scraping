const puppeteer = require('puppeteer');
const moment = require('moment');
const parseString = require('xml2js').parseString;
const readline = require('readline');

const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const scrappingURL = 'https://ecomm.one-line.com/ecom/CUP_HOM_3301.do';

let searchKeyword = 'DELU36202700';

let scrape = async () => {
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    ignoreHTTPSErrors: true,
    headless: true,
    args: [
      '--disable-gpu',
      '--disable-software-rasterizer',
      '--disable-setuid-sandbox',
      '--no-sandbox',
      '--disable-extensions',
      '--remote-debugging-port=9222',
      '--disable-translate'
    ]
  });

  console.log('[---Open Browser---]');
  const page = await browser.newPage();
  await page.setViewport({ width: 1500, height: 3000 });

  console.log('[---Open new Page---]');
  console.log('[---Enter URL---]');
  await page.goto(scrappingURL);

  console.log('[---Wait 2 seconds---]');
  await page.waitFor(2000);

  // Scrape

  await page.screenshot({path: 'nothanks.png'});
  console.log('[---Screenshot Taken---]');

  // Search input
  console.log('[---Search Input box---]');
  const searchInput = await page.$$('#searchName')
  await searchInput[0].type(searchKeyword);

  // Click search button
  console.log('[---Click search button---]');
  const searchButton = await page.$$('#btnSearch');
  await searchButton[0].click();

  // Wait until the result displayed
  console.log('[---Wait 2 seconds---]');
  await page.waitFor(2000);

  const links = await page.$$('.ui-widget-content.jqgrow.ui-row-ltr a')
  await links[0].click();

  // Wait until the result displayed
  console.log('[---Wait 2 seconds---]');
  await page.waitFor(2000);

  await page.screenshot({path: 'Result.png'});
  console.log('[---Screenshot Taken---]');

  // Get all resultTable
  const resultTables = await page.$$('#detailInfo .table_type1');
  console.log('[---tablelength---]', resultTables.length);
  let containerDatas = [];
  for (let i=0; i < resultTables.length; i++) {
    const resultHtml = await page.evaluate(table => table.innerHTML, resultTables[i]);
    containerDatas.push(getContainerData(resultHtml));
  }


  if (containerDatas.length > 0) {
    output = {
      bl: searchKeyword,
      vesselName: containerDatas[0].currentVessel,
      voyageNumber: containerDatas[0].currentVoyage || '',
      eta: containerDatas[0].estimatedTimeOfArrival,
      status: containerDatas[0].status,
      locations: [
        containerDatas[0].original,
        containerDatas[0].currentLocation,
        containerDatas[0].destination
      ]
    }
    console.log('[---output---]', output);
  } else {
    console.log('[---Shipment Not found---]');
  }

  console.log('[---Close browser---]');
  browser.close();
};

r1.question('Input tracking number, container number or BOL number:', (input) => {
  console.log('[---Searching for ---]', input);
  searchKeyword = input.replace(/ONEY/g, '').replace(/oney/g, '');
  if (searchKeyword.length > 4 &&  searchKeyword.substring(0, 4).toLowerCase() === 'oney') {
    searchKeyword = searchKeyword.substring(4, searchKeyword.length);
  }

  if (input) {
    console.log('[---Start scrapping----]');
    scrape().then((value) => {
      console.log(value); // Success!
    });
  }

  r1.close();
})

async function getElementsFrom(page, selector) {
  const ids = await page.evaluate(() => {
    const list = document.querySelectorAll(selector);
    const ids = [];
    for (const element of list) {
      const id = selector + ids.length;
      ids.push(id);
      element.setAttribute('puppeteer', id);
    }
    return ids;
  }, selector);
  const getElements = [];
  for (const id of ids) {
    getElements.push(page.$(`${selector}[puppeteer=${id}]`));
  }
  return Promise.all(getElements);
}

function getText(linkText) {
  linkText = linkText.replace(/\r\n|\r/g, '\n');
  linkText = linkText.replace(/\ +/g, ' ');

  //Replace &nbsp; with a space
  let nbspPattern = new RegExp(String.fromCharCode(160), 'g');
  return linkText.replace(nbspPattern, ' ');
}

function getBillLadingData(tempData) {
  const tempBegin = `<span class="responsiveTd">`.length;
  const vessel = tempData[1].substring(tempBegin, tempData[1].length - 7);
  const portOfLoad = tempData[2].substring(tempBegin, tempData[2].length - 7);
  const portOfDischarge = tempData[3].substring(tempBegin, tempData[3].length - 7);
  const billOfLoadingData = {
    vessel,
    portOfLoad,
    portOfDischarge
  };
  return billOfLoadingData;
}

function getContainerIDData(tempData) {
  const tempBegin = `Container: `.length;
  return tempData.substring(tempBegin, tempData.length);
}

function getContainerData(tempData) {
  let convertedString = tempData.replace(/\r?\n|\r/g, '');
  convertedString = convertedString.slice(convertedString.indexOf('</thead>') + 8);
  convertedString = convertedString.replace(/<br>/g, '<br></br>');
  let resultRows = [];
  
  let estimatedTimeOfArrival = '';
  let currentLocation = '';
  let destination = '';
  let originalLocation = '';
  let currentVessel = '';
  let currentVoyage = '';
  let currentDate;
  let status = 'IN-DELIVERY';

  parseString(convertedString, (err, result) => {
    let trs = result.tbody.tr;
    
    trs = trs.map(item => {
      let tds = item.td;
      const rowData = {
        no: tds[0]._ ? tds[0]._.trim() : '',
        status: tds[1]._ ? tds[1]._.trim() : '',
        location: tds[2]._ ? tds[2]._.trim() : '',
        date: tds[3]._ ? tds[3]._.trim() : ''
      }
      resultRows.push(rowData);
    });
  })

  if (resultRows.length === 0) {
    return null;
  }
  
  originalLocation = resultRows[0].location;
  destination = resultRows[resultRows.length - 1].location;
  estimatedTimeOfArrival = resultRows[resultRows.length - 1].date;

  for (let i=0; i < resultRows.length; i++) {
    let cDate = moment(resultRows[i].date, 'YYYY-MM-DD HH:mm');
    const nDate = new Date();
    if (cDate <= nDate) {
      currentLocation = resultRows[i].location;
      currentDate = cDate;
      const temp11 = resultRows[i].status.split(`\'`);
      if (temp11.length > 1) {
        currentVessel = temp11[1];
        currentVessel = currentVessel.split(' ');
        currentVoyage = currentVessel[currentVessel.length - 1];
        currentVessel = currentVessel.slice(0, currentVessel.length - 1).join(' ');
      }
      if (i === resultRows.length - 1) {
        status = 'COMPLETE';
      }
    }
  }

  estimatedTimeOfArrival = moment(estimatedTimeOfArrival,'YYYY-MM-DD HH:mm').format(moment.HTML5_FMT.DATETIME_LOCAL_MS);
  return {
    estimatedTimeOfArrival,
    currentLocation: splitLocation(currentLocation, true),
    currentVessel,
    currentVoyage,
    destination: splitLocation(destination),
    original: splitLocation(originalLocation),
    status
  };
}

function splitLocation(location, isCurrent) {
  let newLocation = location;
  if (location) {
    newLocation = location.split(',');
    if (isCurrent) {
      if (newLocation.length > 2) {
        return {
          isCurrent: true,
          city: newLocation[0].trim(),
          state: newLocation[1].trim(),
          country: newLocation[2].trim()
        }
      }
      return {
        isCurrent: true,
        city: newLocation[0].trim(),
        state: '',
        country: newLocation[1].trim()
      }
    }
    if (newLocation.length > 2) {
      return {
        city: newLocation[0].trim(),
        state: newLocation[1].trim(),
        country: newLocation[2].trim()
      }
    }
    return {
      city: newLocation[0].trim(),
      state: '',
      country: newLocation[1].trim()
    }
  }
  if (isCurrent) {
    return {
      isCurrent: true,
      location
    }
  }
  return {
    location
  };
}

function xmlToJson(xml) {

	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj['@attributes'] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == 'undefined') {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == 'undefined') {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};